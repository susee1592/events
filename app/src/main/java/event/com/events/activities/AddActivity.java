package event.com.events.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import event.com.events.R;
import event.com.events.database.EventDatabase;
import event.com.events.entities.EventEntity;
import event.com.events.utils.Helper;

public class AddActivity extends AppCompatActivity {
    public static final String status = "status";
    public static final int result_id = 111;
    EditText eventName;
    Button date, time, add;
    EventDatabase database;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        eventName = (EditText) findViewById(R.id.eventName);
        date = (Button) findViewById(R.id.date);
        time = (Button) findViewById(R.id.time);
        add = (Button) findViewById(R.id.add);
        database = new EventDatabase(this);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                DatePickerDialog datePickerDialog = new DatePickerDialog(AddActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String choosenDate = String.format("%02d", dayOfMonth) + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + year;
                                try {
                                    final Calendar cal = Calendar.getInstance();
                                    cal.add(Calendar.DATE, -1);
                                    if (cal.getTime().before(new SimpleDateFormat("dd-MM-yyyy").parse(choosenDate))) {
                                        date.setText(choosenDate);
                                        time.setText(String.format("%02d", mHour) + ":" + String.format("%02d", mMinute));
                                    } else {
                                        Toast.makeText(getBaseContext(), "Choose valid Date!", Toast.LENGTH_LONG).show();
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(AddActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                try {
                                    String choosenTime = String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute);
                                    Date cal = new Date();
                                    if (date.getText().toString().length() == 0) {
                                        Toast.makeText(getBaseContext(), "Please Choose the Date!", Toast.LENGTH_LONG).show();
                                    } else {
                                        if (new SimpleDateFormat("dd-MM-yyyy").format(cal).equalsIgnoreCase(date.getText().toString())) {
                                            if (cal.before(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(date.getText().toString() + " " + choosenTime + ":00"))) {
                                                time.setText(choosenTime);
                                            } else {
                                                Toast.makeText(getBaseContext(), "Choose valid Time!", Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            time.setText(choosenTime);
                                        }
                                    }
                                } catch (ParseException e) {

                                    e.printStackTrace();
                                }
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (eventName.getText().length() == 0) {
                    Snackbar snackbar = Snackbar
                            .make(view, "event name can't be empty!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (date.getText().length() == 0) {
                    Snackbar snackbar = Snackbar
                            .make(view, "date can't be empty!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else if (time.getText().length() == 0) {
                    Snackbar snackbar = Snackbar
                            .make(view, "time can't be empty!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } else {
                    EventEntity entity = new EventEntity();
                    entity.setEventName(eventName.getText().toString());
                    entity.setEventTime(Helper.change_date(date.getText().toString()) + " " + time.getText().toString() + ":00");
                    database.addEvent(entity);

                    Intent intent = new Intent();
                    intent.putExtra(status, "added");
                    setResult(result_id, intent);
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.empty, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
