package event.com.events.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import event.com.events.R;
import event.com.events.adapter.EventAdapter;
import event.com.events.database.EventDatabase;
import event.com.events.entities.EventEntity;

public class MainActivity extends AppCompatActivity {
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    EventDatabase database;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        daily_reminder();

        initial();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                load();
            }
        });
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        load();
                                    }
                                }
        );
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivityForResult(intent, 1);
            }
        });
    }

    public void daily_reminder() {
        Intent myIntent = new Intent(this, NotifyService.class);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, myIntent, 0);

        Calendar remind = Calendar.getInstance();
        Calendar currentCal = Calendar.getInstance();
        //Daily 10.00 AM remain the events
        remind.set(Calendar.HOUR_OF_DAY, 10);
        remind.set(Calendar.MINUTE, 0);
        remind.set(Calendar.SECOND, 0);
        long intendedTime = remind.getTimeInMillis();
        long currentTime = currentCal.getTimeInMillis();

        if (intendedTime >= currentTime) {
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, remind.getTimeInMillis(), 1000 * 60 * 60 * 24, pendingIntent);
        } else {
            remind.add(Calendar.DAY_OF_MONTH, 1);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, remind.getTimeInMillis(), 1000 * 60 * 60 * 24, pendingIntent);
        }
    }

    public void initial() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        recyclerView = (RecyclerView) findViewById(R.id.recycle);
        LinearLayoutManager llm = new LinearLayoutManager(getBaseContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        database = new EventDatabase(this);
        fab = (FloatingActionButton) findViewById(R.id.fab);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AddActivity.result_id) {
            load();
        }
    }

    public void load() {
        List<EventEntity> eventEntities = database.getAllEvent();
        swipeRefreshLayout.setRefreshing(true);
        if (eventEntities.size() == 0) {
            Toast.makeText(getBaseContext(), "No results found!", Toast.LENGTH_LONG).show();
            recyclerView.setAdapter(null);
        } else {
            EventAdapter adapter = new EventAdapter(this, eventEntities);
            recyclerView.setAdapter(adapter);
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView =
                (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() == 0)
                    load();
                else {
                    List<EventEntity> eventEntities = database.searchEvent(newText);
                    if (eventEntities.size() == 0) {
                        recyclerView.setAdapter(null);
                    } else {
                        EventAdapter adapter = new EventAdapter(MainActivity.this, eventEntities);
                        recyclerView.setAdapter(adapter);
                    }
                }
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                List<EventEntity> eventEntities = database.searchEvent(query);
                if (eventEntities.size() == 0) {
                    Toast.makeText(getBaseContext(), "No results found!", Toast.LENGTH_LONG).show();
                    recyclerView.setAdapter(null);
                } else {
                    EventAdapter adapter = new EventAdapter(MainActivity.this, eventEntities);
                    recyclerView.setAdapter(adapter);
                }
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }
}
