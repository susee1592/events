package event.com.events.activities;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.Random;

import event.com.events.R;
import event.com.events.database.EventDatabase;
import event.com.events.entities.EventEntity;
import event.com.events.utils.Helper;

/**
 * Created by suseendran on 5/12/17.
 */

public class NotifyService extends IntentService {

    public NotifyService() {
        super("Notify");
    }

    @Override
    public void onCreate() {

        EventDatabase database = new EventDatabase(this);
        List<EventEntity> eventEntities = database.getCurrentDayEvent();
        for (EventEntity entity : eventEntities) {
            try {
                Random random = new Random();
                int notifyId = random.nextInt(9999 - 1000) + 1000;

                Intent intent = new Intent(this, MainActivity.class);
                PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

                Notification noti = new Notification.Builder(this)
                        .setContentTitle("Remainder for today's event")
                        .setContentText(entity.getEventName() + " | " + Helper.change_format(entity.getEventTime())).setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentIntent(pIntent).build();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                noti.flags |= Notification.FLAG_AUTO_CANCEL;

                notificationManager.notify(notifyId, noti);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        super.onCreate();
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
    }
}
