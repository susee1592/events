package event.com.events.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import event.com.events.R;
import event.com.events.activities.MainActivity;
import event.com.events.database.EventDatabase;
import event.com.events.entities.EventEntity;
import event.com.events.utils.Helper;

/**
 * Created by suseendran on 4/12/17.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.MyViewHolder> {
    private List<EventEntity> eventEntities;
    private Context context;

    public EventAdapter(Context context, List<EventEntity> eventEntities) {
        this.context = context;
        this.eventEntities = eventEntities;
    }

    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_event, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final EventEntity entity = eventEntities.get(position);

        holder.eventName.setText(entity.getEventName());
        holder.eventTime.setText(Helper.change_format(entity.getEventTime()));
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new android.support.v7.app.AlertDialog.Builder(context)
                        .setTitle("Delete Event")
                        .setCancelable(false)
                        .setMessage("Do you want to Delete?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                EventDatabase database = new EventDatabase(context);
                                database.deleteEvent(entity);
                                if (context instanceof MainActivity) {
                                    ((MainActivity) context).load();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return eventEntities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView eventName, eventTime;
        public ImageView delete;

        public MyViewHolder(View view) {
            super(view);
            eventName = (TextView) view.findViewById(R.id.eventName);
            eventTime = (TextView) view.findViewById(R.id.eventTime);
            delete = (ImageView) view.findViewById(R.id.delete);
        }
    }
}