package event.com.events.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import event.com.events.entities.EventEntity;

/**
 * Created by suseendran on 4/12/17.
 */

public class EventDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "event_db";
    private static final String TABLE_EVENT = "event";

    private static final String KEY_ID = "id";
    private static final String KEY_EVENTNAME = "eventName";
    private static final String KEY_EVENTTIME = "eventTime";


    public EventDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_EVENT + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_EVENTNAME + " TEXT," + KEY_EVENTTIME + " DATETIME" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENT);
        onCreate(db);
    }

    public void addEvent(EventEntity entity) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, entity.getId());
        values.put(KEY_EVENTNAME, entity.getEventName());
        values.put(KEY_EVENTTIME, entity.getEventTime());
        db.insert(TABLE_EVENT, null, values);
        db.close();
    }

    public List<EventEntity> getAllEvent() {
        List<EventEntity> eventEntities = new ArrayList<EventEntity>();

        Date date = new Date();
        String selectQuery = "SELECT  * FROM " + TABLE_EVENT + " ORDER BY " + KEY_EVENTTIME + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                EventEntity entity = new EventEntity();
                entity.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                entity.setEventName(cursor.getString(cursor.getColumnIndex(KEY_EVENTNAME)));
                entity.setEventTime(cursor.getString(cursor.getColumnIndex(KEY_EVENTTIME)));

                eventEntities.add(entity);
            } while (cursor.moveToNext());
        }
        return eventEntities;
    }

    public List<EventEntity> getCurrentDayEvent() {
        List<EventEntity> eventEntities = new ArrayList<EventEntity>();

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String curr = format.format(date);
        String selectQuery = "SELECT  * FROM " + TABLE_EVENT + " WHERE " + KEY_EVENTTIME + " like '%" + curr + "%' ORDER BY " + KEY_EVENTTIME + " DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                EventEntity entity = new EventEntity();
                entity.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                entity.setEventName(cursor.getString(cursor.getColumnIndex(KEY_EVENTNAME)));
                entity.setEventTime(cursor.getString(cursor.getColumnIndex(KEY_EVENTTIME)));

                eventEntities.add(entity);
            } while (cursor.moveToNext());
        }
        return eventEntities;
    }


    public List<EventEntity> searchEvent(String name) {
        List<EventEntity> eventEntities = new ArrayList<EventEntity>();
        String selectQuery = "SELECT  * FROM " + TABLE_EVENT + " WHERE (" + KEY_EVENTNAME + " like '%" + name + "%' OR "+KEY_EVENTTIME + " like '%" + name + "%')";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                EventEntity entity = new EventEntity();
                entity.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                entity.setEventName(cursor.getString(cursor.getColumnIndex(KEY_EVENTNAME)));
                entity.setEventTime(cursor.getString(cursor.getColumnIndex(KEY_EVENTTIME)));
                eventEntities.add(entity);
            } while (cursor.moveToNext());
        }
        return eventEntities;
    }

    public void deleteEvent(EventEntity entity) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EVENT, KEY_ID + " = ?",
                new String[]{String.valueOf(entity.getId())});
        db.close();
    }
}
