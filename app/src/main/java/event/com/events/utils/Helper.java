package event.com.events.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by suseendran on 5/12/17.
 */

public class Helper {
    public static String change_date(String input) {
        SimpleDateFormat oldFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = oldFormat.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dateStr = newFormat.format(date);
        return dateStr;
    }

    public static String change_format(String inp) {
        String[] dat = inp.split(" ");
        DateFormat oldFormat = new SimpleDateFormat("hh:mm:ss");
        DateFormat newFormat = new SimpleDateFormat("hh:mm aa");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(oldFormat.parse(dat[1]));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String[] first = dat[1].split(":");
        int u = Integer.parseInt(first[0]);
        switch (u) {
            case 12:
                calendar.set(Calendar.HOUR_OF_DAY, 12);
                break;
            default:
                break;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = sdf.parse(dat[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.applyPattern("dd MMM yyyy");
        String last = sdf.format(d) + " (" + newFormat.format(calendar.getTime()) + ")";

        return last;
    }
}
